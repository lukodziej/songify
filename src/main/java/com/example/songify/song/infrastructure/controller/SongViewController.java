package com.example.songify.song.infrastructure.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.HashMap;
import java.util.Map;

@Controller
public class SongViewController {

    private final Map<Integer, String> database = new HashMap<>();

    @GetMapping("/")
    public String home() {
        return "home.html";
    }

    @GetMapping("/view/songs")
    public String songs(Model model) {
        database.put(1, "shawnmendes song1");
        database.put(2, "arianna grande song");

        model.addAttribute("songMap", database);
        return "songs";
    }




}
