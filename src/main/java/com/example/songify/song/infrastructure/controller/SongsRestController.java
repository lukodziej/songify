package com.example.songify.song.infrastructure.controller;

import com.example.songify.song.domain.model.Song;
import com.example.songify.song.domain.model.SongNotFoundException;
import com.example.songify.song.domain.service.SongAdder;
import com.example.songify.song.domain.service.SongRetriver;
import com.example.songify.song.infrastructure.controller.dto.request.PartialyUpdateSongRequestDto;
import com.example.songify.song.infrastructure.controller.dto.request.PostSongRequestDto;
import com.example.songify.song.infrastructure.controller.dto.request.UpdateSongRequestDto;
import com.example.songify.song.infrastructure.controller.dto.response.*;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping("/songs")
public class SongsRestController {


    private final SongAdder songAdder;
    private final SongRetriver songRetriver;

    public SongsRestController(SongAdder songAdder, SongRetriver songRetriver) {
        this.songAdder = songAdder;
        this.songRetriver = songRetriver;
    }

    @GetMapping
    public ResponseEntity<GetAllSongResponseDto> getAllSongs(@RequestParam(required = false) Integer id) {
        Map<Integer, Song> allSongs = songRetriver.findAll().entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        GetAllSongResponseDto response = new GetAllSongResponseDto(allSongs);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetSongResponseDto> getSongById(@PathVariable Integer id, @RequestHeader(required = false) String requestId) {
        log.info(requestId);
        if (!songRetriver.findAll().containsKey(id)) {
            throw new SongNotFoundException("Song with id " + id + " not found");
        }
        Song song = songRetriver.findAll().get(id);
        GetSongResponseDto response = new GetSongResponseDto(song);
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<CreateSongResponseDto> postSong(@RequestBody @Valid PostSongRequestDto request) {
        Song song = SongMapper.mapFromCreateSongResponseDtoToSong(request);
        songAdder.addSong(song);
        CreateSongResponseDto body = SongMapper.mapFromSongToCreateSongResponseDto(song);
        return ResponseEntity.ok(body);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<DeleteSongResponseDto> deleteSongById(@PathVariable Integer id) {
        if (!songRetriver.findAll().containsKey(id)) {
            throw new SongNotFoundException("Song with id " + id + " not found");
        }
        songRetriver.findAll().remove(id);
        return ResponseEntity.ok(new DeleteSongResponseDto("You deleted song with id: " + id, HttpStatus.OK));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UpdateSongResponseDto> update(@PathVariable Integer id, @RequestBody @Valid UpdateSongRequestDto request) {
        if (!songRetriver.findAll().containsKey(id)) {
            throw new SongNotFoundException("Song with id " + id + " not found");
        }
        String songNameToUpdate = request.songName();
        String artist = request.artist();
        Song newSong = new Song(songNameToUpdate, artist);
        Song oldSongName = songRetriver.findAll().put(id, newSong);
        log.info("Update song with id: " + id + " with oldSongName: " + oldSongName.name() + " to newSongName: " + newSong.name() + " oldArtist: " + oldSongName.artist() +
                " to newArtist: " + newSong.artist());
        return ResponseEntity.ok(new UpdateSongResponseDto(songNameToUpdate));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<PartialyUpdateSongResponseDto> partialyUpdate(@PathVariable Integer id,
                                                                        @RequestBody PartialyUpdateSongRequestDto request) {
        if (!songRetriver.findAll().containsKey(id)) {
            throw new SongNotFoundException("Song with id " + id + " not found");
        }
        Song song = songRetriver.findAll().get(id);
        Song.SongBuilder builder = Song.builder();
        if (request.songName() != null) {
            builder.name(request.songName());
        } else {
            builder.name(song.name());
        }
        if (request.artist() != null) {
            builder.artist(request.artist());
        } else {
            builder.artist(song.artist());
        }
        Song updatedSong = builder.build();
        songRetriver.findAll().put(id, updatedSong);
        return ResponseEntity.ok(new PartialyUpdateSongResponseDto(""));
    }


}
