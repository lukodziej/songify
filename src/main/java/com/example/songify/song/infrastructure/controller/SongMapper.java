package com.example.songify.song.infrastructure.controller;

import com.example.songify.song.infrastructure.controller.dto.request.PostSongRequestDto;
import com.example.songify.song.infrastructure.controller.dto.response.CreateSongResponseDto;
import com.example.songify.song.domain.model.Song;

public class SongMapper {



    public static Song mapFromCreateSongResponseDtoToSong(PostSongRequestDto dto) {
        return new Song(dto.songName(), dto.artist());
    }



    public static CreateSongResponseDto mapFromSongToCreateSongResponseDto(Song song) {
        return new CreateSongResponseDto(song);
    }

}
