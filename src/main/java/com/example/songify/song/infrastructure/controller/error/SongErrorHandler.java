package com.example.songify.song.infrastructure.controller.error;

import com.example.songify.song.infrastructure.controller.SongsRestController;
import com.example.songify.song.infrastructure.controller.dto.response.ErrorSongResponseDto;
import com.example.songify.song.domain.model.SongNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Log4j2
@ControllerAdvice(assignableTypes = SongsRestController.class)
public class SongErrorHandler {

    @ExceptionHandler(SongNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorSongResponseDto handleException(SongNotFoundException exception){
        log.warn("SongErrorHandler");
        return new ErrorSongResponseDto(exception.getMessage(), HttpStatus.NOT_FOUND);
    }
}
