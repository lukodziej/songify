package com.example.songify.song.infrastructure.controller.dto.response;

public record PartialyUpdateSongResponseDto(String message) {
}
