package com.example.songify.song.infrastructure.controller.dto.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record PostSongRequestDto(
        @NotNull(message = "song name must not be null")
        @NotEmpty(message = "songName must not be empty")
        String songName,
        @NotNull(message = "artist name must not be null")
        @NotEmpty(message = "artist must not be empty")
        String artist
){}

